FROM docker.io/hashicorp/terraform:latest as tf

FROM golang:1.17-alpine

COPY --from=tf /bin/terraform /usr/bin/terraform

RUN apk --no-cache add git make
