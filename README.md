# Golang Terraform

A simple update of the golang:1.17-alpine image to include the latest
terraform binary.

This is largely to enable CI testing in
https://gitlab.com/rsrchboy/terraform-provider-gitlabci.  YMMV.
